'use strict';

// Dependancies

let program = require('commander');
let cardata = require('./cardata.json');
let fs = require('fs');
let recursive = require('recursive-readdir');
let path = require('path');
let chalk = require('chalk');

// Commander Definitions for command line interaction
program
  .version('1.0.0')
  .option('-i, --input <path>', 'Folder of un-renamed files')
  .option('-o, --output <path>', 'Location to output files')
  .option('-v, --verbose', 'Outputs verbose information')
  .option('--exterior', 'Renames exterior files')
  .option('--interior', 'Renames interior files')
  .parse(process.argv);

// Checks to make sure program is given correct combinations of flags
if (!program.input || !program.output) {
  console.log(chalk.red('No input and/or output path given. Exiting.'));
  process.exit(1);
} else if (program.exterior && program.interior) {
  console.log(chalk.red('Both interior and exterior flags given. Exiting.'));
  process.exit(1);
} else if (!program.exterior && !program.interior) {
  console.log(chalk.red('Neither interior or exterior flags given. Exiting.'));
  process.exit(1);
} else { // A correct combination of flags is given
  recursive(path.resolve(program.input), (err, files) => {
    if (err) { // Probably can't find the directory
      console.log(chalk.red('Error reading input directory for files'));
    } else {
      files.forEach(file => {
        if (/^(jpeg)|(jpg)|(gif)|(png)$/.test(file.split('.').pop())) {
          var newName = '';
          var oldName = file.split('/').pop();
          var error = false;

          // Splits out car make and SFX code from MVOB
          if (cardata.cardata['RX_300'].indexOf(oldName.split('_')[0]) >= 0) {
            newName += 'RX_300_Sfx_' + oldName.split('_')[0].substring(7);
          } else if (cardata.cardata['RX_350'].indexOf(oldName.split('_')[0]) >= 0) {
            newName += 'RX_350_Sfx_' + oldName.split('_')[0].substring(7);
          } else if (cardata.cardata['RX_350L'].indexOf(oldName.split('_')[0]) >= 0) {
            newName += 'RX_350L_Sfx_' + oldName.split('_')[0].substring(7);
          } else if (cardata.cardata['RX_450h'].indexOf(oldName.split('_')[0]) >= 0) {
            newName += 'RX_450h_Sfx_' + oldName.split('_')[0].substring(7);
          } else if (cardata.cardata['RX_450hL'].indexOf(oldName.split('_')[0]) >= 0) {
            newName += 'RX_450hL_Sfx_' + oldName.split('_')[0].substring(7);
          } else if (cardata.cardata['LS_500'].indexOf(oldName.split('_')[0]) >= 0) {
            newName += 'LS_500_Sfx_' + oldName.split('_')[0].substring(7);
          } else if (cardata.cardata['LS_500h'].indexOf(oldName.split('_')[0]) >= 0) {
            newName += 'LS_500h_Sfx_' + oldName.split('_')[0].substring(7);
          } else {
            error = true;
            // Cannot determine whats going on with the car make from the filename
            console.log(chalk.red('Error determining car make from filename ' + oldName.replace(program.input, '')));
          }
          newName += '_';
          if (program.exterior) { // Exterior 360's
            if (cardata.colourdata[oldName.split('_')[1]]) {
              // Splits out car colour and colour name
              newName += oldName.split('_')[1] + '_' + cardata.colourdata[oldName.split('_')[1]];
              newName += '_' + oldName.split('_')[4].split('.')[0] + '.' + oldName.split('.')[1];
            } else {
              // Couldnt split out car colour
              error = true;
              console.log(chalk.red('Error determining colour of car from filename ' + oldName.replace(program.input, '')));
            }
          } else if (program.interior) { // Interior shotfinder
            if (cardata.trimdata[oldName.split('_')[2]]) {
              // Splits out trim colour and trim code
              newName += oldName.split('_')[2] + '_' + cardata.trimdata[oldName.split('_')[2]];
              newName += '.' + oldName.split('.')[2];
            } else {
              error = true;
              console.log(chalk.red('Error determining trim of car from filename ' + oldName.replace(program.input, '')));
            }
          }

          if (program.verbose) {
            // If verbose is enabled, shows the old and new file names
            console.log(chalk.cyan(file.replace(program.input, '')) + ' ===> ' + chalk.yellow(program.output + newName));
          }

          if (!error) {
            fs.copyFileSync(file, path.resolve(program.output + newName));
          }
        }
      });
    }
  });
}
